package com.nekosith.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import com.nekosith.util.Board;
import com.nekosith.util.Deck;
import com.nekosith.util.JsoupUtil;

@Service
public class MtgGoldFishService {
	public static final String USER_SUBMITTED_DECK_URL_PREFIX = "https://www.mtggoldfish.com/deck/";
	public static final String META_GAME_SAMPLE_DECK_URL_PREFIX = "https://www.mtggoldfish.com/archetype/";
	public static final String ARTICLE_SAMPLE_DECK_URL_PREFIX = "https://www.mtggoldfish.com/articles/";

	public List<Deck> download(String url) throws IOException {
		List<Deck> decks = new ArrayList<Deck>();

		if (url.startsWith(USER_SUBMITTED_DECK_URL_PREFIX)) {
			try {
				Deck deck = getDeckFromUrl(url);
				decks.add(deck);
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("Parse deck from mtggoldfish /deck path fail.");
			}
		} else if (url.startsWith(META_GAME_SAMPLE_DECK_URL_PREFIX)) {
			try {
				Deck deck = getDeckFromUrl(url);
				String title = deck.getTitle().replace(" Deck for Magic: the Gathering","");
				deck.setTitle(title);
				decks.add(deck);
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("Parse deck from mtggoldfish /archetype path fail.");
			}
		} else if (url.startsWith(ARTICLE_SAMPLE_DECK_URL_PREFIX)) {
			try {
				Document doc = JsoupUtil.getInstance().getDocumentByUrl(url);
				List<String> deckIds = getDeckIdFromIns(doc);

				for (String deckId : deckIds) {
					Deck deck = getDeckFromUrl(USER_SUBMITTED_DECK_URL_PREFIX + deckId);
					decks.add(deck);
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("Parse deck from mtggoldfish /articles path fail.");
			}
		} else {
			throw new RuntimeException("Parse deck from mtggoldfish only support /deck, /archetype, /articles.");
		}

		return decks;
	}

	public Deck getDeckFromUrl(String url) throws IOException {
		Document doc = JsoupUtil.getInstance().getDocumentByUrl(url);
		return getDeckFromHidden(doc, url);
	}

	public Deck getDeckFromHidden(Document doc, String url) {
		Deck deck = new Deck();

		String title = doc.select("title").get(0).text();
		deck.setTitle(title);

		deck.setUrl(url);

		Element deckInput = doc.getElementById("deck_input_deck");
		String deckContent = deckInput.val().replace("\r", "");
		String[] boardContents = deckContent.split("sideboard");

		if (boardContents.length > 0) {
			String boardContent = boardContents[0].trim();
			Board main = parseContent(boardContent);
			deck.setMainBoard(main);
		} else {
			Board main = new Board();
			deck.setMainBoard(main);
		}

		if (boardContents.length > 1) {
			String boardContent = boardContents[1].trim();
			Board side = parseContent(boardContent);
			deck.setSideBoard(side);
		} else {
			Board side = new Board();
			deck.setSideBoard(side);
		}

		return deck;
	}

	public Board parseContent(String content) {
		Board board = new Board();
		String[] cardInfos = content.split("\n");

		for (String cardInfo : cardInfos) {
			String[] numberAndCardName = cardInfo.split(" ", 2);

			if (numberAndCardName.length < 2) {
				continue;
			}

			int number = Integer.valueOf(numberAndCardName[0]);
			String cardName = numberAndCardName[1];
			board.addCard(number, cardName);
		}

		return board;
	}

	public List<String> getDeckIdFromIns(Document doc) {
		List<String> deckIds = new ArrayList<String>();

		Elements deckIdInses = doc.select("ins[class=\"widget-deck-placeholder\"]");
		for (Element deckIdIns : deckIdInses) {
			deckIds.add(deckIdIns.attr("data-id"));
		}

		return deckIds;
	}
}
