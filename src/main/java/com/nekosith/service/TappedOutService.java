package com.nekosith.service;

import java.io.IOException;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import com.nekosith.util.Board;
import com.nekosith.util.Deck;
import com.nekosith.util.JsoupUtil;

@Service
public class TappedOutService {
	public static final String DECK_URL_PREFIX = "http://tappedout.net/mtg-decks/";
	public static final String CARD_GROUP_DIV_CLASS = "board-col col-md-4 col-sm-12";
	public static final String CARD_GROUP_TYPE_MAYBEBOARD = "maybeboard";
	public static final String CARD_GROUP_TYPE_SIDEBOARD = "sideboard";
	public static final String NUMBER_CARD_NAME_SEP = "x ";

	public Deck download(String url) throws IOException {
		try {
			Document doc = JsoupUtil.getInstance().getDocumentByUrl(url);
			Elements cardGroupDivs = doc.getElementsByClass(CARD_GROUP_DIV_CLASS);

			Deck deck = new Deck();

			String title = doc.select("title").get(0).text();
			deck.setTitle(title);

			deck.setUrl(url);

			Board main = new Board();
			Board side = new Board();
			for (Element cardGroupDiv : cardGroupDivs) {
				Elements cardGroupTypeH3s = cardGroupDiv.select("h3");
				Elements cardGroupUls = cardGroupDiv.select("ul");
				for (int i = 0; i < cardGroupTypeH3s.size(); ++i) {
					Element cardGroupTypeH3 = cardGroupTypeH3s.get(i);
					Element cardGroupUl = cardGroupUls.get(i);
					String cardGroupType = cardGroupTypeH3.text();
					Board board = main;
					if (null != cardGroupType) {
						if (cardGroupType.toLowerCase().startsWith(CARD_GROUP_TYPE_MAYBEBOARD)) {
							continue;
						} else if (cardGroupType.toLowerCase().startsWith(CARD_GROUP_TYPE_SIDEBOARD)) {
							board = side;
						}
					}
					Elements numberWithCardNameLis = cardGroupUl.select("li");
					for (Element numberWithCardNameLi : numberWithCardNameLis) {
						String[] numberWithCardName = numberWithCardNameLi.text().split(NUMBER_CARD_NAME_SEP, 2);
						if (numberWithCardName.length != 2) {
							continue;
						}
						Integer number = Integer.valueOf(numberWithCardName[0]);
						String cardName = numberWithCardName[1].replace(" / ", " // ");
						board.addCard(number, cardName);
					}
				}
			}
			deck.setMainBoard(main);
			deck.setSideBoard(side);

			return deck;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Parse deck from tappedout fail.");
		}
	}
}
