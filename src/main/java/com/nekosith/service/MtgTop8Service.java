package com.nekosith.service;

import java.io.IOException;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import com.nekosith.util.Board;
import com.nekosith.util.Deck;
import com.nekosith.util.JsoupUtil;

@Service
public class MtgTop8Service {
	public static final String CARD_GROUP_TYPE_SIDEBOARD = "SIDEBOARD";
	public static final String NUMBER_CARD_NAME_SEP = " ";

	public Deck download(String url) throws IOException {
		try {
			Document doc = JsoupUtil.getInstance().getDocumentByUrl(url);
			Element cardGroupTable = doc.select("table[border=0][class=Stable][width=100%]").get(0);

			Deck deck = new Deck();

			String title = doc.select("title").get(0).text().replace(" @ mtgtop8.com", "");
			deck.setTitle(title);

			deck.setUrl(url);

			Board main = new Board();
			Board side = new Board();
			String cardGroupType = null;
			Elements infoTrs = cardGroupTable.select("tr");
			for (Element infoTr : infoTrs) {
				Elements cardGroupTypeTds = infoTr.getElementsByClass("O13");
				if (!cardGroupTypeTds.isEmpty()) {
					cardGroupType = cardGroupTypeTds.get(0).text();
					if (cardGroupType.contains(" ")) {
						cardGroupType = cardGroupType.split(" ")[1];
					}
					continue;
				}
				Elements cardInfoTds = infoTr.getElementsByClass("G14");
				if (!cardInfoTds.isEmpty()) {
					String[] numberWithCardName = cardInfoTds.get(0).select("div").get(0).text().split(NUMBER_CARD_NAME_SEP, 2);
					Integer number = Integer.valueOf(numberWithCardName[0]);
					String cardName = numberWithCardName[1].replace(" / ", " // ");
					Board board = main;
					if ((null != cardGroupType) && cardGroupType.startsWith(CARD_GROUP_TYPE_SIDEBOARD)) {
						board = side;
					}
					board.addCard(number, cardName);
					continue;
				}
			}
			deck.setMainBoard(main);
			deck.setSideBoard(side);

			return deck;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Parse deck from mtgtop8 fail.");
		}
	}
}
