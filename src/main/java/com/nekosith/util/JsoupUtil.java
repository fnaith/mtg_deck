package com.nekosith.util;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class JsoupUtil {
	static private JsoupUtil instance = new JsoupUtil();

	static public JsoupUtil getInstance() {
		return instance;
	}

	public Document getDocumentByUrl(String url) throws IOException {
		Document doc = Jsoup.connect(url).get();
		return doc;
	}
}
