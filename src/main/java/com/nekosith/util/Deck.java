package com.nekosith.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class Deck {
	private String title;
	private String url;
	private Board main;
	private Board side;

	public Deck() {
		setTitle(null);
		setUrl(null);
		setMainBoard(null);
		setSideBoard(null);
	}

	public void setTitle(String title) {
		this.title = title;;
	}

	public String getTitle() {
		return this.title;
	}

	public void setUrl(String url) {
		this.url = url;;
	}

	public String getUrl() {
		return this.url;
	}

	public void setMainBoard(Board main) {
		if (null != main) {
			main.setName("main");
		}

		this.main = main;
	}

	public Board getMainBoard() {
		return main;
	}

	public void setSideBoard(Board side) {
		if (null != side) {
			side.setName("side");
		}

		this.side = side;
	}

	public Board getSideBoard() {
		return side;
	}

	public JsonNode toJsonNode() {
		ObjectMapper objectMapper = new ObjectMapper();
		return toJsonNode(objectMapper);
	}

	public JsonNode toJsonNode(ObjectMapper objectMapper) {
		ObjectNode deck = objectMapper.createObjectNode();

		String title = getTitle();
		if (null != title) {
			deck.put("title", title);
		}

		String url = getUrl();
		if (null != url) {
			deck.put("url", url);
		}

		ArrayNode boards = objectMapper.createArrayNode();
		deck.set("boards", boards);

		Board main = getMainBoard();
		if (null != main) {
			boards.add(main.toJsonNode());
		}

		Board side = getSideBoard();
		if (null != side) {
			boards.add(side.toJsonNode());
		}

		return deck;
	}
}
