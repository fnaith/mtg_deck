package com.nekosith.util;

import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class Board {
	private String name;
	private Map<String, Integer> cardNameToNumber;

	public Board() {
		setName(null);
		cardNameToNumber = new LinkedHashMap<String, Integer>();
	}

	public void setName(String name) {
		this.name = name;;
	}

	public String getName() {
		return this.name;
	}

	public void addCard(int number, String name) {
		Integer total = cardNameToNumber.get(name);

		if (null != total) {
			number += total;
		}

		cardNameToNumber.put(name, number);
	}

	public JsonNode toJsonNode() {
		ObjectMapper objectMapper = new ObjectMapper();
		return toJsonNode(objectMapper);
	}

	public JsonNode toJsonNode(ObjectMapper objectMapper) {
		ObjectNode board = objectMapper.createObjectNode();

		String name = getName();
		if (null != name) {
			board.put("name", name);
		}

		ArrayNode cardInfos = objectMapper.createArrayNode();
		board.set("cardInfos", cardInfos);

		for (Map.Entry<String, Integer> entry : cardNameToNumber.entrySet()) {
			ObjectNode cardInfo = objectMapper.createObjectNode();
			cardInfo.put("number", entry.getValue());
			cardInfo.put("name", entry.getKey());
			cardInfos.add(cardInfo);
		}

		return board;
	}
}
