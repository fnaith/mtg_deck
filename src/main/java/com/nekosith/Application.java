package com.nekosith;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

@Configuration
@EnableAutoConfiguration
@ComponentScan
public class Application {
	public static final HttpStatus REST_RES_STATUS_SUCCESS = HttpStatus.OK;
	public static final HttpStatus REST_RES_STATUS_FAILURE = HttpStatus.SERVICE_UNAVAILABLE;
	public static final String REST_RES_FIELD_DATA = "data";
	public static final String REST_RES_FIELD_ERROR = "error";

	public static ResponseEntity<JsonNode> successResponse(JsonNode jsonNode) {
		ObjectNode result = generateJsonInfo(REST_RES_FIELD_DATA, jsonNode);
		return generateResponse(result, REST_RES_STATUS_SUCCESS);
	}

	public static ResponseEntity<JsonNode> successResponse(String message) {
		return successResponse(new TextNode(message));
	}

	public static ResponseEntity<JsonNode> successResponse(boolean status) {
		return successResponse(status ? BooleanNode.TRUE : BooleanNode.FALSE);
	}

	public static ResponseEntity<JsonNode> successResponse() {
		return successResponse(NullNode.instance);
	}

	public static ResponseEntity<JsonNode> failureResponse(JsonNode jsonNode) {
		ObjectNode result = generateJsonInfo(REST_RES_FIELD_ERROR, jsonNode);
		return generateResponse(result, REST_RES_STATUS_FAILURE);
	}

	public static ResponseEntity<JsonNode> failureResponse(String message) {
		return failureResponse(new TextNode(message));
	}

	private static ObjectNode generateJsonInfo(String field, JsonNode content) {
		ObjectMapper objectMapper = new ObjectMapper();
		ObjectNode result = objectMapper.createObjectNode();
		if ((null != content) && (NullNode.instance != content)) {
			result.set(field, content);
		}
		return result;
	}

	private static ResponseEntity<JsonNode> generateResponse(JsonNode jsonNode, HttpStatus status) {
		try {
			return new ResponseEntity<JsonNode>(jsonNode, status);
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		}
	}
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
