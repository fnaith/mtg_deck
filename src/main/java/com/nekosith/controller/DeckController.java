package com.nekosith.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.nekosith.Application;
import com.nekosith.service.MtgGoldFishService;
import com.nekosith.service.MtgTop8Service;
import com.nekosith.service.TappedOutService;
import com.nekosith.util.Deck;

@RestController
@EnableAutoConfiguration
@RequestMapping("/deck")
public class DeckController {
	@Autowired
	private MtgGoldFishService mtgGoldFishService;
	@Autowired
	private TappedOutService tappedOutService;
	@Autowired
	private MtgTop8Service mtgTop8Service;

	public static final String MTGGOLDFISH_URL_PREFIX = "https://www.mtggoldfish.com/";
	public static final String TAPPEDOUT_URL_PREFIX = "http://tappedout.net/";
	public static final String MTGTOP8_URL_PREFIX = "http://mtgtop8.com/";

	@RequestMapping(value = "/download", method = RequestMethod.POST, produces = "application/json; charset=utf-8", consumes="application/json")
	public ResponseEntity<JsonNode> download(@RequestBody String data) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode json = mapper.readTree(data);
			String url = json.get("url").asText();

			List<Deck> decks = new ArrayList<Deck>();
			if (url.startsWith(MTGGOLDFISH_URL_PREFIX)) {
				decks.addAll(mtgGoldFishService.download(url));;
			} else if (url.startsWith(TAPPEDOUT_URL_PREFIX)) {
				Deck deck = tappedOutService.download(url);
				decks.add(deck);
			} else if (url.startsWith(MTGTOP8_URL_PREFIX)) {
				Deck deck = mtgTop8Service.download(url);
				decks.add(deck);
			}

			ObjectMapper objectMapper = new ObjectMapper();
			ArrayNode result = objectMapper.createArrayNode();
			for (Deck deck : decks) {
				result.add(deck.toJsonNode(objectMapper));
			}

			return Application.successResponse(result);
		} catch (Throwable e) {
			e.printStackTrace();
			return Application.failureResponse(e.toString());
		}
	}
}
